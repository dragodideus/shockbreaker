<?php get_header(); ?>
	<!-- main panel -->
    <div class = "panel panel-default panel-body sb-panel-main">
        <div class="row">
			<?php if (is_active_sidebar('sidebar1')) : ?>
				<div class="col-md-3 sb-separator-right">
					<?php dynamic_sidebar('sidebar1'); ?>
				</div>
			<?php endif; ?>
			
			<?php if (is_active_sidebar('sidebar1')) : ?>
				<div class="col-md-9">
			<?php else: ?>
				<div class="col-md-12">
			<?php endif; ?>
			
			<?php if (have_posts()) : ?>
					<div class="row sb-narrow-pad">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php while(have_posts()) : the_post();?>
							<?php
								get_template_part('content', 'page');
							?>
							<?php endwhile; ?>
						</div>
					</div>
				
					<div class="row sb-narrow-pad">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php
								if ( comments_open() || get_comments_number() )
								{
									comments_template();
								}
							?>
						</div>
					</div>
			<?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>