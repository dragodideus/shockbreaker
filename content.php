<?php if (is_active_sidebar('sidebar1')) : ?>
<div class="col-md-4 col-sm-6 col-xs-12 sb-masonry-item">
<?php else: ?>
<div class="col-md-3 col-sm-4 col-xs-12 sb-masonry-item">
<?php endif; ?>
	<div class="sb-post-standard">
		<?php if ( has_post_thumbnail() ) { ?>
			<a href = "<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', array( 'class'	=> "img-responsive attachment-post-thumbnail")); ?></a>
		<?php } ?>
		<h3><a href = "<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php if ( is_search() OR is_archive() ) { ?>
			<p>
			<?php echo get_the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>">read more&raquo;</a>
			</p>
		<?php } else {
			if ($post->post_excerpt) { ?>
				<p>
				<?php echo get_the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>">read more&raquo;</a>
				</p>
			<?php } else {
				the_content('read more &raquo;');
			}
		} ?>
		<p>
			<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?> @ <?php the_time('F j, Y'); ?></a>
			in
			<?php echo_assigned_category_links(); ?>
		</p>
	</div>
</div>