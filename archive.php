<?php get_header(); ?>
	<!-- main panel -->
    <div class = "panel panel-default panel-body sb-panel-main">
        <div class="row">
			<?php if (is_active_sidebar('sidebar1')) : ?>
				<div class="col-md-3 sb-separator-right">
					<?php dynamic_sidebar('sidebar1'); ?>
				</div>
			<?php endif; ?>
			
			<?php if (is_active_sidebar('sidebar1')) : ?>
				<div class="col-md-9">
			<?php else: ?>
				<div class="col-md-12">
			<?php endif; ?>
			
			<?php if (have_posts()) : ?>
					<h2>
						<?php
							if(is_category())
							{
								//echo 'This is a category';
								single_cat_title();
							}
							elseif(is_tag())
							{
								//echo 'Tag';
								single_tag_title();
							}
							elseif(is_author())
							{
								//echo 'Author';
								the_post();
								echo 'Author Archives: '.get_the_author();
								rewind_posts();
							}
							elseif(is_day())
							{
								//echo 'Day archive';
								echo 'Daily Archives: '.get_the_date();
							}
							elseif(is_month())
							{
								//echo 'Month';
								echo 'Monthly Archives: '.get_the_date('F Y');
							}
							elseif(is_year())
							{
								//echo 'Year';
								echo 'Yearly Archives: '.get_the_date('Y');
							}
							else
							{
								echo 'Archives';
							}
						?>
					</h2>
					<div class="row sb-masonry-container sb-narrow-pad">
						<?php while(have_posts()) : the_post();?>
						<?php
							get_template_part('content',get_post_format());
						?>
						<?php endwhile; ?> 
						<?php if (is_active_sidebar('sidebar1')) : ?>
						<div class="col-md-4 col-sm-6 col-xs-12 sb-masonry-item">
						<?php else: ?>
						<div class="col-md-3 col-sm-4 col-xs-12 sb-masonry-item">
						<?php endif; ?>
							<div class = "sb-post-nav-link">
								<span class="sb-post-nav-link-text"><?php posts_nav_link(' ', 'previous', 'next'); ?></span>
							</div>
						</div>
					</div>
					<!--
					<div class="row sb-narrow-pad">
						<div class="col-md-12 col-sm-12 col-xs-12 sb-navi-right">
							<?php //posts_nav_link(); ?>
						</div>
					</div>
					-->
			<?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>