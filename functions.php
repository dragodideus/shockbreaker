<?php

//register nav menu for bootstrap 3
/**/
require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'shockbreaker' ),
));

//shockbreaker's styles and scripts
function shockbreaker_enqueue_scripts() 
{
	wp_enqueue_style( 'shockbreaker-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );

	wp_enqueue_style( 'shockbreaker-icons', get_template_directory_uri().'/css/font-awesome.min.css' );
	
	wp_enqueue_style( 'shockbreaker-style', get_stylesheet_uri() );
	
	wp_enqueue_script('shockbreaker-jquery', get_template_directory_uri().'/js/jquery-2.1.4.min.js');
	
	wp_enqueue_script('shockbreaker-bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js');
	
	wp_enqueue_script('shockbreaker-masonry', get_template_directory_uri().'/js/masonry.pkgd.min.js');
	
	wp_enqueue_script('shockbreaker-masonry-imagesloaded', get_template_directory_uri().'/js/imagesloaded.pkgd.min.js');
}

add_action( 'wp_enqueue_scripts', 'shockbreaker_enqueue_scripts' );

//shockbreaker setup
function shockbreaker_setup()
{
	//Add featured image
	add_theme_support('post-thumbnails');
	//add_image_size('small-thumbnail', 180, 120, true);
	//add_image_size('banner-image', 920, 210, array('left', 'top'));
	
	//Add post type support
	add_theme_support('post-formats', array('aside', 'link'));
}

add_action('after_setup_theme', 'shockbreaker_setup');

//shockbreaker initialize widgets

function shockbreaker_init_widgets() {
	
	register_sidebar( array(
		'name' => 'Left Sidebar',
		'id' => 'sidebar1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	));
	
	register_sidebar( array(
		'name' => 'Footer Area 1',
		'id' => 'footer1',
		'before_widget' => '<div class="widget-item">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	));
	
	register_sidebar( array(
		'name' => 'Footer Area 2',
		'id' => 'footer2',
		'before_widget' => '<div class="widget-item">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	));
	
	register_sidebar( array(
		'name' => 'Footer Area 3',
		'id' => 'footer3',
		'before_widget' => '<div class="widget-item">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	));
	
	register_sidebar( array(
		'name' => 'Footer Area 4',
		'id' => 'footer4',
		'before_widget' => '<div class="widget-item">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	));
	
}

add_action('widgets_init', 'shockbreaker_init_widgets');

function shockbreaker_customize_register($wp_customize)
{
	$wp_customize->add_section('sb_standard_colors', array(
		'title' => __('Standard Colors', 'Shockbreaker'),
		'priority' => 30,
	));
	
	//background color
	$wp_customize->add_setting('sb_background_color', array(
		'default' => '#111010',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_background_color_control', array(
		'label' => __('Background Color', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_background_color',
	)));
	
	//text color
	$wp_customize->add_setting('sb_text_color', array(
		'default' => '#e7e7e7',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_text_color_control', array(
		'label' => __('Text Color', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_text_color',
	)));
	
	//mini meta text color
	$wp_customize->add_setting('sb_mini_meta_text_color', array(
		'default' => '#777777',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_mini_meta_text_color_control', array(
		'label' => __('Mini Meta Text Color', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_mini_meta_text_color',
	)));
	
	//link post text color
	$wp_customize->add_setting('sb_link_post_text_color', array(
		'default' => '#004982',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_link_post_text_color_control', array(
		'label' => __('Link Post Text Color', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_link_post_text_color',
	)));
	
	//aside post text color
	$wp_customize->add_setting('sb_aside_post_text_color', array(
		'default' => '#cc2030',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_aside_post_text_color_control', array(
		'label' => __('Aside Post Text Color', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_aside_post_text_color',
	)));
	
	//link color link
	$wp_customize->add_setting('sb_link_color_link', array(
		'default' => '#337ab7',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_link_color_link_control', array(
		'label' => __('Link Color: Link', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_link_color_link',
	)));
	
	//link color hover
	$wp_customize->add_setting('sb_link_color_hover', array(
		'default' => '#23527c',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_link_color_hover_control', array(
		'label' => __('Link Color: Hover', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_link_color_hover',
	)));
	
	//link color visited
	$wp_customize->add_setting('sb_link_color_visited', array(
		'default' => '#337ab7',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_link_color_visited_control', array(
		'label' => __('Link Color: Visited', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_link_color_visited',
	)));
	
	//heading colors (h1 to h5)
	$wp_customize->add_setting('sb_heading_colors', array(
		'default' => '#777777',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_heading_colors_control', array(
		'label' => __('Heading Colors', 'Shockbreaker'),
		'section' => 'sb_standard_colors',
		'settings' => 'sb_heading_colors',
	)));
	
	//panel color section
	$wp_customize->add_section('sb_panel_colors', array(
		'title' => __('Panel Colors', 'Shockbreaker'),
		'priority' => 30,
	));
	
	//main panel background color
	$wp_customize->add_setting('sb_main_panel_background_color', array(
		'default' => '#252525',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_main_panel_background_color_control', array(
		'label' => __('Main Panel Background Color', 'Shockbreaker'),
		'section' => 'sb_panel_colors',
		'settings' => 'sb_main_panel_background_color',
	)));
	
	//footer panel background color
	$wp_customize->add_setting('sb_footer_panel_background_color', array(
		'default' => '#252525',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_footer_panel_background_color_control', array(
		'label' => __('Footer Panel Background Color', 'Shockbreaker'),
		'section' => 'sb_panel_colors',
		'settings' => 'sb_footer_panel_background_color',
	)));
	
	//panel border color
	$wp_customize->add_setting('sb_panel_border_color', array(
		'default' => '#dddddd',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_panel_border_color_control', array(
		'label' => __('Panel Border Color', 'Shockbreaker'),
		'section' => 'sb_panel_colors',
		'settings' => 'sb_panel_border_color',
	)));
	
	//separator color
	$wp_customize->add_setting('sb_separator_color', array(
		'default' => '#333333',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_separator_color_control', array(
		'label' => __('Separator Color', 'Shockbreaker'),
		'section' => 'sb_panel_colors',
		'settings' => 'sb_separator_color',
	)));
	
	//standard post background color
	$wp_customize->add_setting('sb_standard_post_background_color', array(
		'default' => '#252525',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_standard_post_background_color_control', array(
		'label' => __('Standard Post Background Color', 'Shockbreaker'),
		'section' => 'sb_panel_colors',
		'settings' => 'sb_standard_post_background_color',
	)));
	
	//aide, link, and nav link post background color
	$wp_customize->add_setting('sb_aside_link_nav_post_background_color', array(
		'default' => '#FFF5CB',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_aside_link_nav_post_background_color_control', array(
		'label' => __('Aside, Link, and Nav Link Post Background Color', 'Shockbreaker'),
		'section' => 'sb_panel_colors',
		'settings' => 'sb_aside_link_nav_post_background_color',
	)));
	
	//menu colors
	$wp_customize->add_section('sb_menu_colors', array(
		'title' => __('Menu Colors', 'Shockbreaker'),
		'priority' => 30,
	));
	
	//menu background color
	$wp_customize->add_setting('sb_menu_background_color', array(
		'default' => '#252525',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_background_color_control', array(
		'label' => __('Menu Background Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_background_color',
	)));
	
	//menu border color
	$wp_customize->add_setting('sb_menu_border_color', array(
		'default' => '#E7E7E7',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_border_color_control', array(
		'label' => __('Menu Border Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_border_color',
	)));
	
	//menu home idle color
	$wp_customize->add_setting('sb_menu_home_idle_color', array(
		'default' => '#777777',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_home_idle_color_control', array(
		'label' => __('Menu Home Idle Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_home_idle_color',
	)));
	
	//menu home hover focus color
	$wp_customize->add_setting('sb_menu_home_hover_focus_color', array(
		'default' => '#5E5E5E',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_home_hover_focus_color_control', array(
		'label' => __('Menu Home Hover Focus Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_home_hover_focus_color',
	)));
	
	//menu link idle color
	$wp_customize->add_setting('sb_menu_link_idle_color', array(
		'default' => '#777777',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_link_idle_color_control', array(
		'label' => __('Menu Link Idle Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_link_idle_color',
	)));
	
	//menu link hover focus color
	$wp_customize->add_setting('sb_menu_link_hover_focus_color', array(
		'default' => '#555555',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_link_hover_focus_color_control', array(
		'label' => __('Menu Link Hover Focus Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_link_hover_focus_color',
	)));
	
	//menu link hover focus background color
	$wp_customize->add_setting('sb_menu_link_hover_focus_background_color', array(
		'default' => '#111010',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_link_hover_focus_background_color_control', array(
		'label' => __('Menu Link Hover Focus Background Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_link_hover_focus_background_color',
	)));
	
	//menu arrow idle color
	$wp_customize->add_setting('sb_menu_arrow_idle_color', array(
		'default' => '#777777',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_arrow_idle_color_control', array(
		'label' => __('Menu Arrow Idle Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_arrow_idle_color',
	)));
	
	//menu arrow hover focus color
	$wp_customize->add_setting('sb_menu_arrow_hover_focus_color', array(
		'default' => '#333333',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_arrow_hover_focus_color_control', array(
		'label' => __('Menu Arrow Hover Focus Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_arrow_hover_focus_color',
	)));
	
	//menu arrow opened color
	$wp_customize->add_setting('sb_menu_arrow_opened_color', array(
		'default' => '#555555',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_arrow_opened_color_control', array(
		'label' => __('Menu Arrow Opened Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_arrow_opened_color',
	)));
	
	//menu toggle border color
	$wp_customize->add_setting('sb_menu_toggle_border_color', array(
		'default' => '#DDDDDD',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_toggle_border_color_control', array(
		'label' => __('Menu Toggle Border Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_toggle_border_color',
	)));
	
	//menu toggle highlight color
	$wp_customize->add_setting('sb_menu_toggle_highlight_color', array(
		'default' => '#DDDDDD',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_toggle_highlight_color_control', array(
		'label' => __('Menu Toggle Highlight Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_toggle_highlight_color',
	)));
	
	//menu toggle icon color
	$wp_customize->add_setting('sb_menu_toggle_icon_color', array(
		'default' => '#cc2030',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_menu_toggle_icon_color_control', array(
		'label' => __('Menu Toggle Icon Color', 'Shockbreaker'),
		'section' => 'sb_menu_colors',
		'settings' => 'sb_menu_toggle_icon_color',
	)));
	
	//widget icon colors
	$wp_customize->add_section('sb_widget_icon_colors', array(
		'title' => __('Widget Icon Colors', 'Shockbreaker'),
		'priority' => 30,
	));
	
	//widget recent entries icon color
	$wp_customize->add_setting('sb_widget_recent_entries_icon_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_widget_recent_entries_icon_color_control', array(
		'label' => __('Widget Recent Entries Icon Color', 'Shockbreaker'),
		'section' => 'sb_widget_icon_colors',
		'settings' => 'sb_widget_recent_entries_icon_color',
	)));
	
	//widget recent comments icon color
	$wp_customize->add_setting('sb_widget_recent_comments_icon_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_widget_recent_comments_icon_color_control', array(
		'label' => __('Widget Recent Comments Icon Color', 'Shockbreaker'),
		'section' => 'sb_widget_icon_colors',
		'settings' => 'sb_widget_recent_comments_icon_color',
	)));
	
	//widget categories icon color
	$wp_customize->add_setting('sb_widget_categories_icon_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_widget_categories_icon_color_control', array(
		'label' => __('Widget Categories Icon Color', 'Shockbreaker'),
		'section' => 'sb_widget_icon_colors',
		'settings' => 'sb_widget_categories_icon_color',
	)));
	
	//form colors
	$wp_customize->add_section('sb_form_colors', array(
		'title' => __('Form Colors', 'Shockbreaker'),
		'priority' => 30,
	));
	
	//button label background color
	$wp_customize->add_setting('sb_button_label_background_color', array(
		'default' => '#ffffff',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_button_label_background_color_control', array(
		'label' => __('Button and Label Background Color', 'Shockbreaker'),
		'section' => 'sb_form_colors',
		'settings' => 'sb_button_label_background_color',
	)));
	
	//button label border color
	$wp_customize->add_setting('sb_button_label_border_color', array(
		'default' => '#252525',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_button_label_border_color_control', array(
		'label' => __('Button and Label Border Color', 'Shockbreaker'),
		'section' => 'sb_form_colors',
		'settings' => 'sb_button_label_border_color',
	)));
	
	//button label hover focus background color
	$wp_customize->add_setting('sb_button_label_hover_focus_background_color', array(
		'default' => '#DDDDDD',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_button_label_hover_focus_background_color_control', array(
		'label' => __('Button and Label Hover Focus Background Color', 'Shockbreaker'),
		'section' => 'sb_form_colors',
		'settings' => 'sb_button_label_hover_focus_background_color',
	)));
	
	//button label hover focus border color
	$wp_customize->add_setting('sb_button_label_hover_focus_border_color', array(
		'default' => '#DDDDDD',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_button_label_hover_focus_border_color_control', array(
		'label' => __('Button and Label Hover Focus Border Color', 'Shockbreaker'),
		'section' => 'sb_form_colors',
		'settings' => 'sb_button_label_hover_focus_border_color',
	)));
	
	//button text color
	$wp_customize->add_setting('sb_button_text_color', array(
		'default' => '#252525',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_button_text_color_control', array(
		'label' => __('Button Text Color', 'Shockbreaker'),
		'section' => 'sb_form_colors',
		'settings' => 'sb_button_text_color',
	)));
	
	//input and textarea text color
	$wp_customize->add_setting('sb_input_textarea_text_color', array(
		'default' => '#252525',
		'transport' => 'refresh',
	));
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sb_input_textarea_text_color_control', array(
		'label' => __('Input and Textarea Text Color', 'Shockbreaker'),
		'section' => 'sb_form_colors',
		'settings' => 'sb_input_textarea_text_color',
	)));
}

add_action('customize_register', 'shockbreaker_customize_register');

// Output Customize CSS
function shockbreaker_customize_css() { ?>
	<style type="text/css">
		/* background color */
		body{
			background-color:<?php echo get_theme_mod('sb_background_color', '#111010'); ?>;
			color:<?php echo get_theme_mod('sb_text_color', '#e7e7e7'); ?>;
		}
		
		/* comment name and "says" */
		.fn, .says{
			/* comment name and 'says' text color */
			color:<?php echo get_theme_mod('sb_text_color', '#e7e7e7'); ?>;
		}

		table#wp-calendar td,table#wp-calendar th{
			/* calendar (table) row bottom color */
			border-bottom:1px solid <?php echo get_theme_mod('sb_background_color', '#111010'); ?>; 
		}

		table tr{
			/* calendar (tablel) bottom-most bottom border color. should same with above */
			border-bottom:1px solid <?php echo get_theme_mod('sb_background_color', '#111010'); ?>; 
		}

		.table-striped>tbody>tr:nth-of-type(odd) {
			/* .table (calendar) stripe color */
			background-color: <?php echo get_theme_mod('sb_background_color', '#111010'); ?>;
		}
		
		/* mini meta text color */
		.sb-mini-meta {
			color:<?php echo get_theme_mod('sb_mini_meta_text_color', '#777777'); ?>;
		}

		.sb-mini-meta a {
			color:<?php echo get_theme_mod('sb_mini_meta_text_color', '#777777'); ?>;
		}
		
		/* link post text color */
		.sb-post-link-text {
			color:<?php echo get_theme_mod('sb_link_post_text_color', '#004982'); ?>;
		}
		
		/* aside post text color */
		.sb-post-aside p{
			color:<?php echo get_theme_mod('sb_aside_post_text_color', '#cc2030'); ?>;
		}
		
		/* link colors */
		a:link{
			color:<?php echo get_theme_mod('sb_link_color_link', '#337ab7'); ?>;
		}

		a:hover{
			color:<?php echo get_theme_mod('sb_link_color_hover', '#23527c'); ?>;
		}

		a:visited{
			color:<?php echo get_theme_mod('sb_link_color_visited', '#337ab7'); ?>;
		}
		
		/* heading colors */
		h1, h2, h3, h4, h5{
			color:<?php echo get_theme_mod('sb_heading_colors', '#777777'); ?>;
		}
		
		/* main panel background color */
		.sb-panel-main{
			
			background-color:<?php echo get_theme_mod('sb_main_panel_background_color', '#252525'); ?>;
		}
		
		/* footer panel background color */
		.sb-panel-footer{
			
			background-color:<?php echo get_theme_mod('sb_footer_panel_background_color', '#252525'); ?>;
		}
		
		/* panel border color */
		.panel-default{
			border-color:<?php echo get_theme_mod('sb_panel_border_color', '#dddddd'); ?>;
		}
		
		/* separator */
		.widget ul > li,
		.widget ol  > li{
			border-bottom: 1px solid <?php echo get_theme_mod('sb_separator_color', '#333333'); ?>; /* widget content list bottom color */
		}
		
		.comment-body{
			/* comment list separator color */
			border-bottom: 1px solid <?php echo get_theme_mod('sb_separator_color', '#333333'); ?>;
		}
		
		.sb-separator-right {
			/* separator between sidebar and main post area color */
			border-right: 1px solid <?php echo get_theme_mod('sb_separator_color', '#333333'); ?>;
		}

		/* standard post background color */
		.sb-post-standard{
			background-color:<?php echo get_theme_mod('sb_standard_post_background_color', '#252525'); ?>;
			border: 1px solid #dddddd;
		}
		
		/* aside, link, and nav link background color */
		.sb-post-aside,
		.sb-post-link ,
		.sb-post-nav-link{
			background-color:<?php echo get_theme_mod('sb_aside_link_nav_post_background_color', '#FFF5CB'); ?>;
		}
		
		/* menu background and border color */
		.navbar-default {
			background-color:<?php echo get_theme_mod('sb_menu_background_color', '#252525'); ?>; /* color of navbar background */
			border-color:<?php echo get_theme_mod('sb_menu_border_color', '#E7E7E7'); ?>; /* color of navbar border */
		}
		
		/* dropdown popup background color */
		.dropdown-menu {
			/* color of popup menu when menu item dropped down */
			background-color:<?php echo get_theme_mod('sb_menu_background_color', '#252525'); ?>;
		}
		
		/* menu home */
		.navbar-default .navbar-brand {
			/* navbar brand (e.g. Home) color when idle */
			color:<?php echo get_theme_mod('sb_menu_home_idle_color', '#777777'); ?>;
		}

		.navbar-default .navbar-brand:hover,
		.navbar-default .navbar-brand:focus {
			/* navbar brand (e.g. Home) color when hovered or focused */
			color:<?php echo get_theme_mod('sb_menu_home_hover_focus_color', '#5E5E5E'); ?>;
		}
		
		/* menu link */
		.navbar-default .navbar-nav > li > a {
			/* menu link color when idle */
			color:<?php echo get_theme_mod('sb_menu_link_idle_color', '#777777'); ?>;
		}
		.navbar-default .navbar-nav > li > a:hover,
		.navbar-default .navbar-nav > li > a:focus {
			/* when navbar item hovered or focused */
			color:<?php echo get_theme_mod('sb_menu_link_hover_focus_color', '#555555'); ?>; 
			background-color:<?php echo get_theme_mod('sb_menu_link_hover_focus_background_color', '#111010'); ?>;
		}
		.navbar-default .navbar-nav > .active > a, 
		.navbar-default .navbar-nav > .active > a:hover, 
		.navbar-default .navbar-nav > .active > a:focus {
			/* when navbar item active */
			color:<?php echo get_theme_mod('sb_menu_link_hover_focus_color', '#555555'); ?>;
			background-color:<?php echo get_theme_mod('sb_menu_link_hover_focus_background_color', '#111010'); ?>;
		}
		.navbar-default .navbar-nav > .open > a, 
		.navbar-default .navbar-nav > .open > a:hover, 
		.navbar-default .navbar-nav > .open > a:focus {
			/* when dropdown focused */
			color:<?php echo get_theme_mod('sb_menu_link_hover_focus_color', '#555555'); ?>;
			background-color:<?php echo get_theme_mod('sb_menu_link_hover_focus_background_color', '#111010'); ?>;
		}
		
		/* mobile menu color */
		@media (max-width: 767px) {
			.navbar-default .navbar-nav .open .dropdown-menu > li > a {
				/* link color of dropdown popup when in mobile view idle */
				color:<?php echo get_theme_mod('sb_menu_link_idle_color', '#777777'); ?>;
				background-color:<?php echo get_theme_mod('sb_menu_background_color', '#252525'); ?>;
			}
			.navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
			.navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
				/* link color of dropdown popup when in mobile view hover or focus */
				color:<?php echo get_theme_mod('sb_menu_link_hover_focus_color', '#555555'); ?>;
				background-color:<?php echo get_theme_mod('sb_menu_link_hover_focus_background_color', '#111010'); ?>;
			}
			.navbar-default .navbar-nav .open .dropdown-menu > .active > a,
			.navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
			.navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
				/* color of dropdown selected item when in mobile view */
				color:<?php echo get_theme_mod('sb_menu_link_hover_focus_color', '#555555'); ?>;
				background-color:<?php echo get_theme_mod('sb_menu_link_hover_focus_background_color', '#111010'); ?>;
			}
		}
		/* dropdown menu link */
		
		.dropdown-menu > li > a {
			/* dropdown item link color when idle */
			color:<?php echo get_theme_mod('sb_menu_link_idle_color', '#777777'); ?>;
		}
		.dropdown-menu > li > a:hover,
		.dropdown-menu > li > a:focus {
			/* when dropdown item is hovered or focused */
			color:<?php echo get_theme_mod('sb_menu_link_hover_focus_color', '#555555'); ?>;
			background-color:<?php echo get_theme_mod('sb_menu_link_hover_focus_background_color', '#111010'); ?>;
		}

		.dropdown-menu > .active > a,
		.dropdown-menu > .active > a:hover,
		.dropdown-menu > .active > a:focus {
			/* when dropdown item is selected */
			color:<?php echo get_theme_mod('sb_menu_link_hover_focus_color', '#555555'); ?>;
			background-color:<?php echo get_theme_mod('sb_menu_link_hover_focus_background_color', '#111010'); ?>;
		}
		
		/* menu arrow */
		.navbar-default .navbar-nav > .dropdown > a .caret {
			border-top-color:<?php echo get_theme_mod('sb_menu_arrow_idle_color', '#777777'); ?>; /* dropdown arrow color when idle */
			border-bottom-color:<?php echo get_theme_mod('sb_menu_arrow_idle_color', '#777777'); ?>; /* unknown, but make it equals with above */
		}
		.navbar-default .navbar-nav > .dropdown > a:hover .caret,
		.navbar-default .navbar-nav > .dropdown > a:focus .caret {
			border-top-color:<?php echo get_theme_mod('sb_menu_arrow_hover_focus_color', '#333333'); ?>; /* dropdown arrow color when hover or focus */
			border-bottom-color:<?php echo get_theme_mod('sb_menu_arrow_hover_focus_color', '#333333'); ?>; /* unknown, but make it equals with above */
		}
		.navbar-default .navbar-nav > .open > a .caret, 
		.navbar-default .navbar-nav > .open > a:hover .caret, 
		.navbar-default .navbar-nav > .open > a:focus .caret {
			/* arrow color when dropdown opened */
			border-top-color:<?php echo get_theme_mod('sb_menu_arrow_opened_color', '#555555'); ?>;
			border-bottom-color:<?php echo get_theme_mod('sb_menu_arrow_opened_color', '#555555'); ?>;
		}
		
		/* menu toggle - mobile version */
		.navbar-default .navbar-toggle {
			/* menu toggle (something with striped icon on top right) border color in mobile view when idle - menu toggle border*/
			border-color:<?php echo get_theme_mod('sb_menu_toggle_border_color', '#DDDDDD'); ?>;
		}
		.navbar-default .navbar-toggle:hover,
		.navbar-default .navbar-toggle:focus {
			/* menu toggle (something with striped icon on top right) background color in mobile view when hover or focus - menu toggle highlight*/
			background-color:<?php echo get_theme_mod('sb_menu_toggle_highlight_color', '#DDDDDD'); ?>;
		}
		.navbar-default .navbar-toggle .icon-bar {
			/* menu toggle (something with striped icon on top right) icon color in mobile view - menu toggle icon*/
			background-color:<?php echo get_theme_mod('sb_menu_toggle_icon_color', '#cc2030'); ?>;
		}
		
		/* widget icon colors */
		.widget_recent_entries ul li:before {
			color:<?php echo get_theme_mod('sb_widget_recent_entries_icon_color', '#ffffff'); ?>;/* font awesome icon color */
		}

		.widget_recent_comments ul li:before {
			color:<?php echo get_theme_mod('sb_widget_recent_comments_icon_color', '#ffffff'); ?>;/* font awesome icon color */
		}

		.widget_categories ul li:before {
			color:<?php echo get_theme_mod('sb_widget_categories_icon_color', '#ffffff'); ?>;/* font awesome icon color */
		}
		
		/* Button color */
		.btn-default, .label-default {
		  background-color:<?php echo get_theme_mod('sb_button_label_background_color', '#ffffff'); ?>; /* button background color */
		  border-color:<?php echo get_theme_mod('sb_button_label_border_color', '#252525'); ?>; /* button border color */
		}
		.btn-default:hover, .label-default[href]:hover, .label-default[href]:focus, .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, #image-navigation .nav-previous a:hover, #image-navigation .nav-next a:hover {
		  /* button color on hover. both should same */
		  background-color:<?php echo get_theme_mod('sb_button_label_hover_focus_background_color', '#DDDDDD'); ?>;
		  border-color:<?php echo get_theme_mod('sb_button_label_hover_focus_border_color', '#DDDDDD'); ?>;
		}
		.btn.btn-default {
			color:<?php echo get_theme_mod('sb_button_text_color', '#252525'); ?>; /* button text color */
		}
		
		/* input */
		input[type="text"], textarea {
			color:<?php echo get_theme_mod('sb_input_textarea_text_color', '#252525'); ?>;
		}
	</style>
<?php }

add_action('wp_head', 'shockbreaker_customize_css');

function echo_assigned_category_links($separator = ", ")
{
	$categories = get_the_category();
	//$separator = ", ";
	$output = '';
	
	if($categories)
	{
		foreach($categories as $category)
		{
			$output .= '<a href="'.get_category_link($category->term_id).'">'.$category->cat_name.'</a>'.$separator;
		}
		echo trim($output, $separator);
	}
}

function get_comment_form_args()
{
	$args = array(
		'id_form'           => 'commentform',
		'id_submit'         => 'submit',
		'class_submit'      => 'submit btn btn-default',
		'name_submit'       => 'submit',
		'title_reply'       => __( 'Leave a Reply' ),
		'title_reply_to'    => __( 'Leave a Reply to %s' ),
		'cancel_reply_link' => __( 'Cancel Reply' ),
		'label_submit'      => __( 'Post Comment' ),
		'format'            => 'html5',

		'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) .
		'</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true">' .
		'</textarea></p>',

		'must_log_in' => '<p class="must-log-in">' .
		sprintf(
		  __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
		  wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
		) . '</p>',

		'logged_in_as' => '<p class="logged-in-as">' .
		sprintf(
		__( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
		  admin_url( 'profile.php' ),
		  $user_identity,
		  wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
		) . '</p>',

		'comment_notes_before' => '<p class="comment-notes">' .
		__( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) .
		'</p>',

		'comment_notes_after' => '<p class="form-allowed-tags">' .
		sprintf(
		  __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ),
		  ' <code>' . allowed_tags() . '</code>'
		) . '</p>',
	);
	
	return $args;
}