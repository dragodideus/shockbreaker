	<!-- footer -->
	<?php if (is_active_sidebar('footer1') OR is_active_sidebar('footer2') OR
				is_active_sidebar('footer3') OR is_active_sidebar('footer4')) : ?>
		<footer class = "panel panel-default panel-body sb-panel-footer">
			<div class="row">
				<?php if (is_active_sidebar('footer1')) : ?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?php dynamic_sidebar('footer1'); ?>
					</div>
				<?php endif; ?>
				
				<?php if (is_active_sidebar('footer2')) : ?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?php dynamic_sidebar('footer1'); ?>
					</div>
				<?php endif; ?>
				
				<?php if (is_active_sidebar('footer3')) : ?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?php dynamic_sidebar('footer3'); ?>
					</div>
				<?php endif; ?>
				
				<?php if (is_active_sidebar('footer4')) : ?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?php dynamic_sidebar('footer4'); ?>
					</div>
				<?php endif; ?>
			</div>
		</footer>
	<?php endif; ?>
	<p><?php bloginfo('name'); ?> - &copy; <?php echo date('Y');?></p>
</div>
<script>
$(document).ready(function() {
	$('table#wp-calendar').addClass('table table-striped');
	var $container = $('.sb-masonry-container');
	var $grid;
	$container.imagesLoaded( function () {
		$grid = $container.masonry({
			columnWidth: '.sb-masonry-item',
			itemSelector: '.sb-masonry-item'
		});
	});
});
</script>
</body>
</html>