<h1><?php the_title(); ?></h1>
<p>
	<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?> @ <?php the_time('F j, Y'); ?></a>
	in
	<?php echo_assigned_category_links(); ?>
</p>
<p><?php the_content(''); ?></p>