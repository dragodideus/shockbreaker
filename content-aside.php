<?php if (is_active_sidebar('sidebar1')) : ?>
<div class="col-md-4 col-sm-6 col-xs-12 sb-masonry-item">
<?php else: ?>
<div class="col-md-3 col-sm-4 col-xs-12 sb-masonry-item">
<?php endif; ?>
	<div class = "sb-post-aside">
		<p class="sb-mini-meta"><a href="<?php the_permalink(); ?>"><?php the_author(); ?> @ <?php the_time(get_option('date_format')); ?></a></p>
		<p><?php the_content(); ?></p>
	</div>
</div>