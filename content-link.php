<?php if (is_active_sidebar('sidebar1')) : ?>
<div class="col-md-4 col-sm-6 col-xs-12 sb-masonry-item">
<?php else: ?>
<div class="col-md-3 col-sm-4 col-xs-12 sb-masonry-item">
<?php endif; ?>
	<div class = "sb-post-link">
		<a href="<?php echo get_the_content(); ?>" rel="nofollow" target="_blank">
		<span class="sb-mini-meta"><?php the_author(); ?> @ <?php the_time(get_option('date_format')); ?></span>
		<span class="sb-post-link-text"><?php the_title(); ?></span>
		</a>
	</div>
</div>